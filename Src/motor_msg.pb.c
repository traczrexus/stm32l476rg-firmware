/* Automatically generated nanopb constant definitions */
/* Generated by nanopb-0.3.9-dev at Mon Mar 12 19:25:16 2018. */

#include "motor_msg.pb.h"

/* @@protoc_insertion_point(includes) */
#if PB_PROTO_HEADER_VERSION != 30
#error Regenerate this file with the current version of nanopb generator.
#endif



const pb_field_t motor_msg_fields[3] = {
    PB_FIELD(  1, FLOAT   , REQUIRED, STATIC  , FIRST, motor_msg, desired_position, desired_position, 0),
    PB_FIELD(  2, FLOAT   , REQUIRED, STATIC  , OTHER, motor_msg, max_velocity, desired_position, 0),
    PB_LAST_FIELD
};


/* @@protoc_insertion_point(eof) */
