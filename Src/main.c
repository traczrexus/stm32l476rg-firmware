
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "cmsis_os.h"

/* USER CODE BEGIN Includes */
#include <pb_encode.h>
#include <pb_decode.h>
#include <sensors_msg.pb.h>
#include <motor_msg.pb.h>
#include <valves_msg.pb.h>

#define MOTOR_STOP 0
#define MOTOR_FORWARD 1
#define MOTOR_BACK 2

#define VALVE_ON 1
#define VALVE_OFF 0

#define FRAME_FISRT_PART 1
#define FRAME_SECOND_PART_MOTOR 2
#define FRAME_SECOND_PART_VALVE 3
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;
DMA_HandleTypeDef hdma_adc1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim5;

UART_HandleTypeDef huart1;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;

osThreadId commTaskHandle;
osThreadId motorTaskHandle;
osThreadId encoderTaskHandle;
osThreadId valveTaskHandle;
osThreadId pressureTaskHandle;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
uint8_t rx_buffer[20] = "\0";
float motor_desired_position = 0;
float motor_max_velocity = 0;
uint8_t motor_duty = 50;

uint8_t motor_state = MOTOR_STOP;

float position;
float velocity;

uint8_t valve1 = VALVE_OFF;
uint8_t valve1_msg = VALVE_OFF;
float valve1_open_time = 0; //ms
uint8_t  valve2 = VALVE_OFF;
uint8_t  valve2_msg = VALVE_OFF;
float valve2_open_time = 0; //ms
float valve_actual_time = 0;//ms


float PRESS_SENS[1];

uint8_t frame_state = FRAME_FISRT_PART;
uint16_t msg_size = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM5_Init(void);
static void MX_ADC1_Init(void);
void StartCommTask(void const * argument);
void StartMotorTask(void const * argument);
void StartEncoderTask(void const * argument);
void StartValveTask(void const * argument);
void StartPressureTask(void const * argument);
                                    
void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
                                

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
//	  HAL_GPIO_TogglePin(D6_GPIO_Port, D6_Pin);

	    if(frame_state == FRAME_FISRT_PART)
	     {
			if (rx_buffer[0] == 0xff)
			{
			    if(rx_buffer[1] == 0x01)
			    {
					msg_size= rx_buffer[2];
//					HAL_UART_Receive_DMA(&huart1, rx_buffer, msg_size);
					frame_state = FRAME_SECOND_PART_MOTOR;
			    }
			    if(rx_buffer[1] == 0x02)
			    {
					msg_size = rx_buffer[2];
//					HAL_UART_Receive_DMA(&huart1, rx_buffer, msg_size);
					frame_state = FRAME_SECOND_PART_VALVE;
			    }
	      }
			if(frame_state == FRAME_SECOND_PART_MOTOR )
		{
		      motor_msg motor_msg_decoded = motor_msg_init_default;
		      pb_istream_t stream_decoded = pb_istream_from_buffer(rx_buffer, msg_size);
		      pb_decode(&stream_decoded,motor_msg_fields, &motor_msg_decoded);

		      motor_desired_position = motor_msg_decoded.desired_position;
		      motor_max_velocity = motor_msg_decoded.max_velocity;
//		      HAL_UART_Receive_DMA(&huart1, rx_buffer, 3);
		      frame_state = FRAME_FISRT_PART;
		}
			if(frame_state == FRAME_SECOND_PART_VALVE)
		{
		      valves_msg valves_msg_decoded = valves_msg_init_default;
		      pb_istream_t stream_decoded = pb_istream_from_buffer(rx_buffer+3, msg_size);
		      pb_decode(&stream_decoded,valves_msg_fields, &valves_msg_decoded);

		      valve1_msg = (uint8_t)valves_msg_decoded.valve1_open;
		      valve1_open_time = valves_msg_decoded.valve1_open_time;
		      valve2_msg = (uint8_t)valves_msg_decoded.valve2_open;
		      valve2_open_time = valves_msg_decoded.valve2_open_time;
			  valve_actual_time = 0;

		      //		      HAL_UART_Receive_DMA(&huart1, rx_buffer, 3);
		      frame_state = FRAME_FISRT_PART;
		}
//			else
//			  {
//			      frame_state = FRAME_FISRT_PART;
//			  }

	      }

}
/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();
  MX_TIM5_Init();
  MX_ADC1_Init();
  /* USER CODE BEGIN 2 */

  HAL_ADC_Start(&hadc1 );


  HAL_GPIO_WritePin(D5_GPIO_Port,D5_Pin,1);
  HAL_GPIO_WritePin(D6_GPIO_Port,D6_Pin,1);
  HAL_GPIO_WritePin(D8_GPIO_Port,D8_Pin,1);

  HAL_TIMEx_PWMN_Start(&htim1, TIM_CHANNEL_1);

  HAL_UART_Receive_DMA(&huart1, rx_buffer, 17);

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* Create the thread(s) */
  /* definition and creation of commTask */
  osThreadDef(commTask, StartCommTask, osPriorityNormal, 0, 100);
  commTaskHandle = osThreadCreate(osThread(commTask), NULL);

  /* definition and creation of motorTask */
  osThreadDef(motorTask, StartMotorTask, osPriorityNormal, 0, 110);
  motorTaskHandle = osThreadCreate(osThread(motorTask), NULL);

  /* definition and creation of encoderTask */
  osThreadDef(encoderTask, StartEncoderTask, osPriorityNormal, 0, 110);
  encoderTaskHandle = osThreadCreate(osThread(encoderTask), NULL);

  /* definition and creation of valveTask */
  osThreadDef(valveTask, StartValveTask, osPriorityNormal, 0, 110);
  valveTaskHandle = osThreadCreate(osThread(valveTask), NULL);

  /* definition and creation of pressureTask */
  osThreadDef(pressureTask, StartPressureTask, osPriorityIdle, 0, 80);
  pressureTaskHandle = osThreadCreate(osThread(pressureTask), NULL);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */
 

  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 2;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 2;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* ADC1 init function */
static void MX_ADC1_Init(void)
{

  ADC_MultiModeTypeDef multimode;
  ADC_ChannelConfTypeDef sConfig;

    /**Common config 
    */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV8;
  hadc1.Init.Resolution = ADC_RESOLUTION_12B;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc1.Init.LowPowerAutoWait = DISABLE;
  hadc1.Init.ContinuousConvMode = ENABLE;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.NbrOfDiscConversion = 1;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.Overrun = ADC_OVR_DATA_OVERWRITTEN;
  hadc1.Init.OversamplingMode = DISABLE;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the ADC multi-mode 
    */
  multimode.Mode = ADC_MODE_INDEPENDENT;
  if (HAL_ADCEx_MultiModeConfigChannel(&hadc1, &multimode) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure Regular Channel 
    */
  sConfig.Channel = ADC_CHANNEL_9;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_2CYCLES_5;
  sConfig.SingleDiff = ADC_SINGLE_ENDED;
  sConfig.OffsetNumber = ADC_OFFSET_NONE;
  sConfig.Offset = 0;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* TIM1 init function */
static void MX_TIM1_Init(void)
{

  TIM_MasterConfigTypeDef sMasterConfig;
  TIM_OC_InitTypeDef sConfigOC;
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig;

  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 49;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 99;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_ENABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim1);

}

/* TIM5 init function */
static void MX_TIM5_Init(void)
{

  TIM_Encoder_InitTypeDef sConfig;
  TIM_MasterConfigTypeDef sMasterConfig;

  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 0;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 0;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI1;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim5, &sConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void)
{

  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Channel1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel1_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel1_IRQn);
  /* DMA1_Channel4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel4_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
  /* DMA1_Channel5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Channel5_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(DMA1_Channel5_IRQn);

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, MT_IN1_Pin|VALVE1_EN_Pin|VALVE2_EN_Pin|D5_Pin 
                          |D4_Pin|D6_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LD2_Pin|D8_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(D3_GPIO_Port, D3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, MT_IN2_Pin|D1_Pin|D2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : MT_IN1_Pin VALVE1_EN_Pin VALVE2_EN_Pin D5_Pin 
                           D4_Pin D6_Pin */
  GPIO_InitStruct.Pin = MT_IN1_Pin|VALVE1_EN_Pin|VALVE2_EN_Pin|D5_Pin 
                          |D4_Pin|D6_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : P2_Pin P1_Pin P4_Pin P3_Pin */
  GPIO_InitStruct.Pin = P2_Pin|P1_Pin|P4_Pin|P3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : LD2_Pin D8_Pin */
  GPIO_InitStruct.Pin = LD2_Pin|D8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : D3_Pin */
  GPIO_InitStruct.Pin = D3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(D3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : MT_IN2_Pin D1_Pin D2_Pin */
  GPIO_InitStruct.Pin = MT_IN2_Pin|D1_Pin|D2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* StartCommTask function */
void StartCommTask(void const * argument)
{

  /* USER CODE BEGIN 5 */
	  sensor_msg msg_encoded = sensor_msg_init_zero;
	  uint8_t tx_buffer[50] = "\0";
	  tx_buffer[0] = 0xFF;
	  size_t message_length;

  /* Infinite loop */
  for(;;)
  {
	  if(HAL_GPIO_ReadPin(P1_GPIO_Port, P1_Pin)==GPIO_PIN_SET)
	  {
	      valve1 = VALVE_ON;
	  }
	  else
	    {
	      valve1 = VALVE_OFF;
	    }

	  if(HAL_GPIO_ReadPin(P2_GPIO_Port, P2_Pin)==GPIO_PIN_SET)
	  {
	      valve2 = VALVE_ON;
		  valve_actual_time = 0;

	  }
	  else
	    {
	      valve2 = VALVE_OFF;
		  valve_actual_time = 0;

	    }



	  msg_encoded.pressure = PRESS_SENS[0];
	  msg_encoded.motor_actual_position = position;
	  msg_encoded.motor_actual_velocity = velocity;
	  msg_encoded.battery_level = motor_duty;


	  pb_ostream_t stream = pb_ostream_from_buffer(&tx_buffer[2] , sizeof(tx_buffer)-2);
	  pb_encode(&stream, sensor_msg_fields, &msg_encoded);
	  message_length = stream.bytes_written;
	  tx_buffer[1] = (uint8_t)message_length;
	  HAL_UART_Transmit_DMA(&huart1,  tx_buffer,  message_length+2);

	  HAL_GPIO_TogglePin(D8_GPIO_Port, D8_Pin);
	  HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);


	  osDelay(100);
  }
  /* USER CODE END 5 */ 
}

/* StartMotorTask function */
void StartMotorTask(void const * argument)
{
  /* USER CODE BEGIN StartMotorTask */
	uint8_t motor_stop_time = 50; //ms
	uint8_t motor_time_step = 10; //ms
	uint8_t motor_time_iter = 0;
  /* Infinite loop */
  for(;;)
  {




	  if(HAL_GPIO_ReadPin(P3_GPIO_Port, P3_Pin)==GPIO_PIN_SET)
	  {
		  motor_state = MOTOR_FORWARD;
		  motor_time_iter = 0;
	  }

	  if(HAL_GPIO_ReadPin(P4_GPIO_Port, P4_Pin)==GPIO_PIN_SET)
	  {
		  motor_state = MOTOR_BACK;
		  motor_time_iter = 0;
	  }

//	  if(HAL_GPIO_ReadPin(P2_GPIO_Port, P2_Pin)==GPIO_PIN_SET)
//	  {
//		  motor_state = MOTOR_STOP;
//		  motor_time_iter = 0;
//	  }

	  if(motor_stop_time<motor_time_iter)
	  {
		  motor_state = MOTOR_STOP;
	  }

	  if(motor_state == MOTOR_STOP)
	  {
		  HAL_GPIO_WritePin(MT_IN1_GPIO_Port, MT_IN1_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(MT_IN2_GPIO_Port, MT_IN2_Pin, GPIO_PIN_RESET);
		  TIM1->CCR1 = 0;
	  }
	  else if(motor_state == MOTOR_FORWARD)
	  {
		  HAL_GPIO_WritePin(MT_IN1_GPIO_Port, MT_IN1_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(MT_IN2_GPIO_Port, MT_IN2_Pin, GPIO_PIN_RESET);
		  TIM1->CCR1 = motor_duty;
	  }
	  else if(motor_state == MOTOR_BACK)
	  {
		  HAL_GPIO_WritePin(MT_IN1_GPIO_Port, MT_IN1_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(MT_IN2_GPIO_Port, MT_IN2_Pin, GPIO_PIN_SET);
		  TIM1->CCR1 = motor_duty;
	  }
	motor_time_iter += motor_time_step;
    osDelay(motor_time_step);
  }
  /* USER CODE END StartMotorTask */
}

/* StartEncoderTask function */
void StartEncoderTask(void const * argument)
{
  /* USER CODE BEGIN StartEncoderTask */

	uint32_t dt = 50;	// in miliseconds

	uint32_t encoderCounterValue = 0;
    uint32_t encoderPrevCounterValue = 0;
    uint32_t encoderCounterDiff = 0;

    float distance_per_impulse = 0.001;		// In [meters] - TODO - to be determined!!!
    position = (float)encoderCounterValue * distance_per_impulse;

    float previousPosition = 0;

  /* Infinite loop */
	for(;;)
	{
		encoderCounterValue = TIM5->CNT;
		encoderCounterDiff = encoderCounterValue - encoderPrevCounterValue;


		if (encoderCounterValue < encoderPrevCounterValue)
		{
			encoderCounterDiff = encoderCounterValue - encoderPrevCounterValue;

			if (encoderCounterDiff < -5000)
			{
				// We overflow ARR, so in fact we moved forward
				encoderCounterDiff = TIM5->ARR + encoderCounterDiff;
			}
			else
			{
				// We keep encoderCounterDiff, because motor just moved backwards
			}
		}
		else
		{
			encoderCounterDiff = encoderCounterValue - encoderPrevCounterValue;

			if (encoderCounterDiff > 5000)
			{
				// We overflow ARR, so in fact we moved backwards
				encoderCounterDiff =  encoderCounterDiff - TIM5->ARR;
			}
			else
			{
				// We keep frontLeftEncoderDiff, because motor just moved forward
			}
		}

		encoderPrevCounterValue = encoderCounterValue;
		previousPosition = position;
		position += ((float)encoderCounterDiff * distance_per_impulse);
		velocity = (position - previousPosition) * 0.001 / dt;	// 0.001 to convert from [m/ms] to [m/s]

		// WARNING - TODO - osDelay isn't accurate, other mechanism to calculate velocity required

		osDelay(dt);
	}
  /* USER CODE END StartEncoderTask */
}

/* StartValveTask function */
void StartValveTask(void const * argument)
{
  /* USER CODE BEGIN StartValveTask */
  uint8_t valve_time_step = 1; //ms
  /* Infinite loop */
  for(;;)
  {
//      if(valve1_msg == VALVE_OFF)
//	{
//	  valve1 = VALVE_OFF;
//	}
//      else if(valve1_open_time == 0 && valve1_msg == VALVE_ON )
//	{
//		valve1 = VALVE_ON;
//	}
//      else
//	{
//	    if(valve1_open_time > valve_actual_time)
//	      {
//		valve1 = VALVE_ON;
//	      }
//	    else
//	      {
//		valve1 = VALVE_OFF;
//	      }
//      }
//
//      if(valve2_msg == VALVE_OFF)
//	{
//	  valve2 = VALVE_OFF;
//	}
//      else if(valve2_open_time == 0 && valve2_msg == VALVE_ON )
//	{
//		valve2 = VALVE_ON;
//	}
//      else
//	{
//	    if(valve2_open_time > valve_actual_time)
//	      {
//		valve2 = VALVE_ON;
//	      }
//	    else
//	      {
//		valve2 = VALVE_OFF;
//	      }
//      }


	  if(valve1 == VALVE_ON)
	  {
		  HAL_GPIO_WritePin(D1_GPIO_Port, D1_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(VALVE1_EN_GPIO_Port, VALVE1_EN_Pin, GPIO_PIN_SET);
	  }
	  else
	  {
		  HAL_GPIO_WritePin(D1_GPIO_Port, D1_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(VALVE1_EN_GPIO_Port, VALVE1_EN_Pin, GPIO_PIN_RESET);
	  }

	   if(valve2 == VALVE_ON)
	  {
		   HAL_GPIO_WritePin(D2_GPIO_Port, D2_Pin, GPIO_PIN_SET);
		  HAL_GPIO_WritePin(VALVE2_EN_GPIO_Port, VALVE2_EN_Pin, GPIO_PIN_SET);
	  }
	  else
	  {
		  HAL_GPIO_WritePin(D2_GPIO_Port, D2_Pin, GPIO_PIN_RESET);
		  HAL_GPIO_WritePin(VALVE2_EN_GPIO_Port, VALVE2_EN_Pin, GPIO_PIN_RESET);
	  }

	  valve_actual_time += valve_time_step;
    osDelay(valve_time_step);
  }
  /* USER CODE END StartValveTask */
}

/* StartPressureTask function */
void StartPressureTask(void const * argument)
{
  /* USER CODE BEGIN StartPressureTask */
  /* Infinite loop */
  for(;;)
  {
	  PRESS_SENS[0] = (3.55*HAL_ADC_GetValue(&hadc1))/4096; // ADC -> V
	  PRESS_SENS[0] = (PRESS_SENS[0]*1000)/180; // V->I
	  PRESS_SENS[0] = (PRESS_SENS[0] - 4)/16; // I-> pressure

    osDelay(10);
  }
  /* USER CODE END StartPressureTask */
}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM17 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM17) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
